The names don't make any sense, except the c and d
for client and daemon although neither are clients
nor daemon.

This is 2 simple shell scripts to log activity on
a computer (`tsd`) and access it without manually
using `grep` or `gnuplot` (`tsc`).

Note : `tsd` is supposed to be run from a crontab.
It works in conjunction of my build of `dwmblocks`
which itself works only under dwm. You can suppress
the `pkill` line if you use Wayland or not DWM.

Also note : `tsc` needs GNU date to work (I'm not
aware of any other tool that does what I make it
do)
